/*                amiga-fdisk -  Amiga RDB Fdisk for Linux
 *                 amigastuff.h part  -  function includes
 *                   written in 1996 by Stefan Reinauer
 *
 *    Copyright (C) 1996-1998 by Stefan Reinauer
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 1, or (at your option)
 *    any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA
 */

extern int get_dev,list_only;
extern char disk_device[256];

int override(void);
