# Makefile -- Makefile for amiga-fdisk
#
# Copyright '96-'98 Stefan Reinauer (stepan@linux.de)
#

CC=             gcc
CFLAGS=         -O2 -fomit-frame-pointer -Wall -I./include
#CFLAGS=         -g -fomit-frame-pointer -Wall -I./include -DDEBUG
LDFLAGS=        # -s -N
LIBS=		-lreadline

# Where to put binaries?
# See the "install" rule for the links. . .

SBIN= 		amiga-fdisk

# Where to put datebase files?

all: $(SBIN)

amiga-fdisk: amigastuff.o fdisk.o
	$(CC) $(LDFLAGS) $^ $(LIBS) -o $@

install: all
	$(INSTALLDIR) $(SBINDIR)
	$(INSTALLBIN) $(SBIN) $(SBINDIR)

clean:
	-rm -f *.o *~ core $(SBIN)

tidy:
	-rm -f *.o *~ core 
	-strip amiga-fdisk
